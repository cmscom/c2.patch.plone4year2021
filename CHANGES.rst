Changelog
=========

1.0 (2020-11-17)
------------------

- No change from the previous release.
  [terapyon]

1.0a2 (2020-11-12)
------------------

- Bug fix for non sub version, eg) 4.2.
  [terapyon]

1.0a1 (2020-11-10)
------------------

- Initial release.
  [terapyon]
