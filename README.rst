.. This README is meant for consumption by humans and pypi. Pypi can render rst files so please do not use Sphinx features.
   If you want to learn more about writing documentation, please check out: http://docs.plone.org/about/documentation_styleguide.html
   This text does not appear on pypi or github. It is a comment.

=======================
c2.patch.plone4year2021
=======================

Installation
------------

Install c2.patch.plone4year2021 by adding it to your buildout::

    [buildout]

    ...

    eggs =
        c2.patch.plone4year2021


and then running ``bin/buildout``


Contribute
----------

- Issue Tracker: https://bitbucket.org/cmscom/c2.patch.plone4year2021/issues
- Source Code: https://bitbucket.org/cmscom/c2.patch.plone4year2021


License
-------

The project is licensed under the GPLv2.
